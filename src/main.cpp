/**
 * main.cpp but but containing only C code, except the vector<string> parameters
 *
 * performing some basic tests with the CanModule CWrapper.
 * For sending and receiving use two instances in parallel with arguments "send" and "receive".
 * The wrapper is limited to 8 connections.
 */

#include <main.h>
#include <canmodule_wrapper.h>

int main ( int argc, char **argv ){
	printf("CanModule cwrapper test VERSION %s %d %s\n", __FILE__, __LINE__, __DATE__);
	bool direction;
	if ( argc == 2 ){
		printf("got arguments %s %s\n", argv[ 0 ], argv[ 1 ]);
		if ( strcmp(argv[ 1 ], "send") == 0 ) {
			direction = true;
		} else if ( strcmp(argv[ 1 ], "receive") == 0 ) {
			direction = false;
		} else {
			printf("usage: %s [ send | receive ]\n", argv[ 0 ]);
			exit (0);
		}
	} else {
		printf("usage: %s [ send | receive ]\n", argv[ 0 ]);
		exit (0);
	}

	vector<string> canmoduleParameters_an_0 = {"an", "0" /* can */, "128.141.159.194" /* ip */ , "250000 0 1 0 0 1" };
	vector<string> canmoduleParameters_an_1 = {"an", "1" /* can */, "128.141.159.194" /* ip */ , "250000 0 1 0 0 1" };
	vector<string> canmoduleParameters_st_0_0 = {"st", "0" /* can */, "0" /* usb */, "Unspecified" };
	vector<string> canmoduleParameters_st_0_1 = {"st", "0" /* can */, "0" /* usb */, "Unspecified" };

	CanMessage cm0;
	cm0.c_id = 1;
	cm0.c_data[0] = 11;

	CanMessage cm1;
	cm1.c_id = 2;
	cm1.c_data[0] = 22;

	printf("init 0\n");
	canmodule_init( 0, Log::TRC, canmoduleParameters_an_0 );
	printf("init 1\n");
	canmodule_init( 1, Log::TRC, canmoduleParameters_an_1 );
//#if 0
	printf("init 2\n");
	canmodule_init( 2, Log::TRC, canmoduleParameters_st_0_0 );
	printf("init 3\n");
	canmodule_init( 3, Log::TRC, canmoduleParameters_st_0_1 );
//#endif


	if ( direction ){
		printf("send 0\n");
		canmodule_send( 0, cm0 );
		printf("send 1\n");
		canmodule_send( 1, cm1 );
//#if 0
		printf("send 2\n");
		canmodule_send( 2, cm0 );
		printf("send 3\n");
		canmodule_send( 3, cm1 );
//#endif

	} else {
		// should be an extra thread
		CanMessage msg0 = canmodule_waitForNewCanMessage( 0 );
		printf("canmodule_waitForNewCanMessage OK on 0: id= %ld\n", msg0.c_id);
		CanMessage msg1 = canmodule_waitForNewCanMessage( 1 );
		printf("canmodule_waitForNewCanMessage OK on 1: id= %ld\n", msg1.c_id);
	}
	canmodule_delete( 0 );
	canmodule_delete( 1 );

	return ( 0 );
}
