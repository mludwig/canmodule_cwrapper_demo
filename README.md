# CanModule_Cwrapper_demo

non-GUI demo code in C to exercise and test CanModule through the C-wrapper. 
We use the binary distribution of CanModule (libs & headers from nexus CanModule.tgz) 
It should work for 
* multiple CAN ports on the same single module open at the same time 
* several modules of the same type with multiple ports open on each
* several modules from same and different types
* windows and linux
