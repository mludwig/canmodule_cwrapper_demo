# toolchain for cc7 canmodule_cwrapper-demo
# mludwig at cern dot ch
# cmake -DCMAKE_TOOLCHAIN_FILE=build_pcbe13632_cc7.cmake .
#
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CC7 build for canmodule_cwrapper")
# 
SET ( BOOST_PATH_LIBS "/home/mludwig/3rdPartySoftware/boost/vanilla_1_59_0/stage/lib" )
SET ( BOOST_HEADERS   "/home/mludwig/3rdPartySoftware/boost/vanilla_1_59_0" )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_program_options 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_PATH_LIBS= ${BOOST_PATH_LIBS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_LIBS= ${BOOST_LIBS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_HEADERS= ${BOOST_HEADERS} ")

