# toolchain for cc7 canmodule_cwrapper-demo
# mludwig at cern dot ch
# cmake -DCMAKE_TOOLCHAIN_FILE=jenkins_cc7.cmake .
#
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CC7 build for canmodule_cwrapper")
#
SET ( BOOST_PATH_LIBS "$ENV{JENKINSWS}/boost/lib" )
SET ( BOOST_HEADERS   "$ENV{JENKINSWS}/boost/include" )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_PATH_LIBS= ${BOOST_PATH_LIBS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_LIBS= ${BOOST_LIBS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_HEADERS= ${BOOST_HEADERS} ")
 
